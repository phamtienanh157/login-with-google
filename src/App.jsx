import { Route, Switch, BrowserRouter } from "react-router-dom";
import PrivateRoute from "./navigation/PrivateRoute";
import Login from "./page/Login";
import Home from "./page/Home";

export default function App() {
  return (
    <BrowserRouter>
      <Switch>
        <Route path="/login" component={Login} />
        <PrivateRoute path="/" component={Home} />
      </Switch>
    </BrowserRouter>
  );
}
