import { createSlice } from '@reduxjs/toolkit'
import { getAccessToken } from "../utils/localStorage"

const initialState = {
    accessToken: getAccessToken(),
}

export const authSlice = createSlice({
    name: 'login',
    initialState,
    reducers: {
        setAccessToken: (state, action) => {
            state.accessToken = action.payload
        }
    },
})

export const { setAccessToken } = authSlice.actions

export default authSlice.reducer